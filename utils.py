import yaml
import logging
import os
import pathlib
from elasticsearch import Elasticsearch, NotFoundError
from elasticsearch.client import IndicesClient

LOGGER = logging.getLogger()

PATH_TO_PARENT = pathlib.Path(__file__).parent.absolute()
PATH_TO_CONFIG_FILE = os.path.join(PATH_TO_PARENT, 'config.yaml')
with open(PATH_TO_CONFIG_FILE, 'r') as file:
    CONFIG = yaml.safe_load(file)

def update_mappings(index):
    with open(os.path.join(PATH_TO_PARENT, CONFIG['PATH_TO_MAPPINGS']), 'r') as file:
        mappings = yaml.safe_load(file)
    with open(os.path.join(PATH_TO_PARENT, CONFIG['PATH_TO_SETTINGS']), 'r') as file:
        settings = yaml.safe_load(file)

    es = Elasticsearch([{'host': CONFIG['ES_HOST'], 'port': CONFIG['ES_PORT']}])
    ind_client = IndicesClient(es)
    new_mappings = {
        'mappings': mappings,
        'settings': settings
    }
    try:
        ind_client.delete(index)
    except NotFoundError:
        LOGGER.warning(f'Index {index} does not exist. Creating index.')
        pass
    status = ind_client.create(index, body=new_mappings)
    return status