class PostProcessor:
    """
    A class to post process a search result for courses
    To use:
        es_res = Elasticsearch().search(...)
        # For autocomplete feature
        res = PostProcessor.process(es_res, True)
        # Fpr regular search
        res = PostProcessor.process(es_res, False, fields=['title'])

    """
    def __init__(self):
        pass

    def _post_process_hits(self, es_res, fields=None):
        """Post process the response from the search engine.

        Parameters
        ----------
        es_res : dict
            response from the search engine after making a request to /_search
        fields : list
            What fields to include. Valid fields are the ones defined in mappings.
            If not specified, all fields will be returned

        Returns
        -------
        hit_count : int
            The number of hits
        hits : list
            A list of dict. Each dict represents an item.
        """
        hit_count = es_res['hits']['total']['value']

        if fields is None:
            hits = [hit['_source'] for hit in es_res['hits']['hits']]
            return hit_count, hits

        hits = []
        for hit in es_res['hits']['hits']:
            item = {}
            for field in fields:
                item[field] = hit['_source'][field]
            hits.append(item)
        return hit_count, hits

    def _post_process_did_you_mean(self, es_res):
        """Post process the "did you mean" part of the response from elasticsearch

        The order of precedence of the type of the suggested items are:
        1. agu_code
        2. title
        3. topics and skills

        Parameters
        ----------
        es_res : dict
            response from the search engine after making a request to /_search

        Returns
        -------
        dict
        """
        es_res = es_res['suggest']
        agu_code_options = es_res['agu_code_did_you_mean'][0]['options']
        agu_code_options = [option['text'] for option in agu_code_options]
        title_options = es_res['title_did_you_mean'][0]['options']
        title_options = [option['text'] for option in title_options]
        topic_skill_options = es_res['topics_and_skills_did_you_mean'][0]['options']
        topic_skill_options = [option['text'] for option in topic_skill_options]

        if agu_code_options:
            return agu_code_options
        if title_options:
            return title_options
        return topic_skill_options

    def _post_process_aggregations(self, es_res):
        """Post process the aggregations part of response from the search engine.

        Parameters
        ----------
        es_res : dict
            response from the search engine after making a request to /_search

        Returns
        -------
        hit_count : int
            The number of hits
        hits : list
            A list of dict. Each dict represents an item.
        """
        es_res = es_res['aggregations']
        aggregations = {}
        aggregations['content_format'] = {bucket['key']: bucket['doc_count']
                                          for bucket in es_res['content_format']['buckets']}
        aggregations['platform'] = {bucket['key']: bucket['doc_count']
                                    for bucket in es_res['platform']['buckets']}
        aggregations['content_length_range'] = {bucket['key']: bucket['doc_count']
                                                for bucket in es_res['content_length_range']['buckets']}
        return aggregations

    def post_process_search(self, es_res, fields=None):
        """Post process regular search (as opposed to the auto completion search)

        Parameters
        ----------
        es_res : dict
            response from the search engine after making a request to /_search
        fields : list
            What fields to include. Valid fields are the ones defined in mappings.
            If not specified, all fields will be returned

        Returns
        -------
        dict
        """
        hit_count, hits = self._post_process_hits(es_res, fields)
        aggregations = self._post_process_aggregations(es_res)
        did_you_mean = self._post_process_did_you_mean(es_res)
        return {'hit_count': hit_count, 'hits': hits, 'aggregations': aggregations, 'did_you_mean': did_you_mean}

    def post_process_auto_complete(self, es_res):
        """Post process an auto completion search response

        Parameters
        ----------
        es_res : dict
            response from the search engine after making a request to /_search

        Returns
        -------
        dict
        """
        es_res = es_res['suggest']
        agu_code_options = es_res['agu_code_auto_com'][0]['options']
        agu_code_options = [option['text'] for option in agu_code_options]
        title_options = es_res['title_auto_com'][0]['options']
        title_options = [option['text'] for option in title_options]
        topic_skill_options = es_res['skill_and_topic_auto_com'][0]['options']
        topic_skill_options = [option['text'] for option in topic_skill_options]

        auto_com_options = {
            'agu_code_options': agu_code_options,
            'title_options': title_options,
            'topic_skill_options': topic_skill_options
        }
        return auto_com_options

    def process(self, es_res, is_auto_complete, fields=None):
        """Method to post processing the response from Elasticsearch

        Parameters
        ----------
        es_re : dict
            response from the search engine after making a request to /_searchs
        is_auto_complete : bool
            Whether it is for the auto-complete feature
        fields : list
            What fields to include. Valid fields are the ones defined in mappings.
            If not specified, all fields will be returned

        Returns
        -------
        dict
        """
        if is_auto_complete:
            return self.post_process_auto_complete(es_res)
        else:
            return self.post_process_search(es_res, fields=fields)