import os
import re
import copy
import logging
import pathlib

import yaml
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk, BulkIndexError

LOGGER = logging.getLogger()

PATH_TO_CONFIG_FILE = os.path.join(pathlib.Path(__file__).parent.absolute(), 'config.yaml')
with open(PATH_TO_CONFIG_FILE, 'r') as file:
    CONFIG = yaml.safe_load(file)

class PathwayIndexer:
    """
    A class for indexing courses for PATHWAY
    To use:
        indexer = PathwayIndexer()
        indexer.index(docs_to_index)
    """
    def __init__(self):
        self._es_client = Elasticsearch(hosts=[{'host': CONFIG['ES_HOST'], 'port': CONFIG['ES_PORT']}])

    def _pre_process(self, docs):
        for doc in docs:
            es_doc = copy.deepcopy(doc)
            es_doc['title'] = self._remove_parentheses(doc['title'])
            es_doc['topics'] = self._remove_topics(doc['topics'], CONFIG['TOPICS_TO_REMOVE'])
            es_doc['agu_code_auto_com'] = doc['agu_code']
            es_doc['title_auto_com'] = doc['title']
            es_doc['skill_and_topic_auto_com'] = es_doc['topics'][:]
            if doc['skills']:
                es_doc['skill_and_topic_auto_com'].append(doc['skills'][0]['lvl1skill'])
                es_doc['skill_and_topic_auto_com'].append(doc['skills'][0]['lvl2skill'])
                es_doc['skill_and_topic_auto_com'].append(doc['skills'][0]['lvl3skill'])
                es_doc['lvl1skill'] = doc['skills'][0]['lvl1skill']
                es_doc['lvl2skill'] = doc['skills'][0]['lvl2skill']
                es_doc['lvl3skill'] = doc['skills'][0]['lvl3skill']
            es_doc.pop('skills')
            es_doc.pop('content_id')
            es_doc.pop('content_level')

            body = {
                '_index': CONFIG['COURSE_INDEX'],
                '_id': es_doc['agu_code'],
                '_source': es_doc
            }
            yield body

    def _remove_topics(self, topics, topics_to_remove):
        topics_to_remove = set(topics_to_remove)
        topics = set([topic.lower() for topic in topics])
        return list(topics - topics_to_remove)

    def _remove_parentheses(self, s):
        paren_pattern = re.compile(r'\(.*?\)')
        s = paren_pattern.sub('', s)
        s = s.strip()
        return s

    def index(self, docs):
        """Method used to index documents

        Parameters
        ----------
        docs : list
            A list of dicts each of which represents a course.
            Please refer to `mappings.yaml` for the keys that each dict should contain

        Returns
        -------
        status : tuple+
            Number of successfully executed actions and either list of errors
        """
        actions = self._pre_process(docs)
        try:
            status = bulk(self._es_client, actions)
        except BulkIndexError as e:
            LOGGER.error(e)
        return status