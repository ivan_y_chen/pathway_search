import logging
import os
import pathlib

import yaml
from elasticsearch import Elasticsearch

LOGGER = logging.getLogger()

PATH_TO_CONFIG_FILE = os.path.join(pathlib.Path(__file__).parent.absolute(), 'config.yaml')
with open(PATH_TO_CONFIG_FILE, 'r') as file:
    CONFIG = yaml.safe_load(file)

class PathwaySearcher:
    def __init__(self):
        self._es_client = Elasticsearch(hosts=[{'host': CONFIG['ES_HOST'], 'port': CONFIG['ES_PORT']}])

    def search(self, user_query, is_auto_complete, must_not=None, filters=None, post_filters=None, size=10):
        query = QueryMaker().make_query(user_query, is_auto_complete,
                                        must_not=must_not, filters=filters, post_filters=post_filters, size=size)
        self._es_client.search(index=CONFIG['COURSE_INDEX'], body=query)

class QueryMaker:
    """
    A class to produce query for elasticsearch given an user query
    To use:
        query_maker = QueryMaker()
        # make a query for the autocompleting feature:
        query_maker.make_query(user_query, True)
        # make a query for the regular search
        query_maker.make_query(user_query, False)
        # make a query for bowsing without any user query
        query_maker.make_query('', False, filters={'lvl1skill': ['Applied Processes']})
    """
    def __init__(self):
        pass
    def _make_search_query(self, user_query):
        """Make a search query based on the user's query (`user_query`). Namely, the `query` part of the request body.

        The fields to search against included are: agu_code, title, topics, skills and description.

        Parameters
        ----------
        user_query : str
            Input from the user.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        if not user_query:
            query = {
                'query': {
                    'bool': {
                        'should': [
                            {'match_all': {}}
                        ]
                    }
                }
            }
            return query
        query = {
            'query': {
                'bool': {
                    'should': [
                        {
                            'term': {
                                'agu_code': {
                                    'value': user_query,
                                    'boost': 100
                                }
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': [
                                    'agu_code.prefixed',
                                    'agu_code.prefixed_without_digits'
                                ],
                                'type': 'most_fields'
                            }
                        },
                        {
                            'match_phrase': {
                                'title': {
                                    'query': user_query,
                                    'boost': 100
                                }
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['title', 'title.bigrammed^5'],
                                'type': 'most_fields',
                                'boost': 10
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['topics', 'topics.bigrammed'],
                                'type': 'most_fields',
                                'boost': 5
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['lvl1skill', 'lvl1skill.bigrammed',
                                           'lvl2skill', 'lvl2skill.bigrammed',
                                           'lvl3skill', 'lvl3skill.bigrammed'],
                                'type': 'most_fields',
                                'boost': 5
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['title',
                                           'title.bigrammed',
                                           'topics',
                                           'topics.bigrammed',
                                           'lvl1skill',
                                           'lvl1skill.bigrammed',
                                           'lvl2skill',
                                           'lvl2skill.bigrammed',
                                           'lvl3skill',
                                           'lvl3skill.bigrammed',
                                           'description',
                                           'description.bigrammed',
                                           'description.trigrammed',
                                           'agu_code.prefixed',
                                           'agu_code.prefixed_without_digits',
                                           'platform.analyzed^50'],
                                'type': 'cross_fields',
                                'tie_breaker': 1
                            }
                        }
                    ],
                    'minimum_should_match': 2
                }
            }
        }
        return query

    def _make_auto_com_query(self, user_query):
        """Make a query for the auto completion feature based on the user's query (`user_query`).

        The fields that used to provide data for the auto completion feature are: title, agu_code, topics and skills.

        Parameters
        ----------
        user_query : str
            Input from the user.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        query = {
            'suggest': {
                'title_auto_com': {
                    'prefix': user_query,
                    'completion': {
                        'field': 'title_auto_com',
                        'size': 3,
                        'skip_duplicates': True
                    }
                },
                'agu_code_auto_com': {
                    'prefix': user_query,
                    'completion': {
                        'field': 'agu_code_auto_com',
                        'size': 3,
                        'skip_duplicates': True
                    }
                },
                'skill_and_topic_auto_com': {
                    'prefix': user_query,
                    'completion': {
                        'field': 'skill_and_topic_auto_com',
                        'size': 3,
                        'skip_duplicates': True
                    }
                }
            }
        }
        return query

    def _make_did_you_mean_query(self, user_query):
        """Make a query for the search query suggestion feature based on the user's query (`user_query`).

        The query requests for search query suggestion based on four of the fields of the documents in ES, agu_code,
        title, topics and skills. The suggested words have the following properties:
            - (Share the same prefix (longer than 3 characters) with the word entered by the user. OR
            - Share the same suffix (longer than 3 characters) with the word entered by the user.) AND
            - Edit distance between the word entered by the user and the suggested word is no more than 2.

        Parameters
        ----------
        user_query : str
            Input from the user.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        query = {
            'suggest': {
                'text': user_query,
                'agu_code_did_you_mean': {
                    'phrase': {
                        'field': 'agu_code.did_you_mean',
                        'size': 3,
                        'direct_generator': [
                            {
                                'field': 'agu_code.did_you_mean',
                                'suggest_mode': 'always',
                                'min_word_length': 3,
                                'prefix_length': 3
                            },
                            {
                                'field': 'agu_code.did_you_mean_reversed',
                                'suggest_mode': 'always',
                                'pre_filter': 'did_you_mean_agu_code_reversed',
                                'min_word_length': 3,
                                'prefix_length': 3
                            }
                        ],
                        'collate': {
                            'query': {
                                'source': {
                                    'term': {
                                        'agu_code': {
                                            'value': '{{suggestion}}'
                                        }
                                    }
                                }
                            },
                            'prune': False
                        },
                        'smoothing': {
                            "stupid_backoff": {
                                "discount": 0.4
                            }
                        }
                    }
                },
                'title_did_you_mean': {
                    'phrase': {
                        'field': 'title.with_stopwords',
                        'size': 3,
                        'direct_generator': [
                            {
                                'field': 'title.with_stopwords',
                                'suggest_mode': 'always',
                                'min_word_length': 3,
                                'prefix_length': 3
                            },
                            {
                                'field': 'title.reversed',
                                'suggest_mode': 'always',
                                'pre_filter': 'reversed',
                                'post_filter': 'reversed',
                                'min_word_length': 3,
                                'prefix_length': 3
                            }
                        ],
                        'collate': {
                            'query': {
                                'source': {
                                    'match': {
                                        'title.with_stopwords': '{{suggestion}}'
                                    }
                                }
                            },
                            'prune': False
                        },
                        'smoothing': {
                            "stupid_backoff": {
                                "discount": 0.4
                            }
                        }
                    }
                },
                'topics_and_skills_did_you_mean': {
                    'phrase': {
                        'field': 'topics_and_skills.did_you_mean',
                        'size': 3,
                        'direct_generator': [
                            {
                                'field': 'topics_and_skills.did_you_mean',
                                'suggest_mode': 'always',
                                'min_word_length': 3,
                                'prefix_length': 3
                            },
                            {
                                'field': 'topics_and_skills.did_you_mean_reversed',
                                'suggest_mode': 'always',
                                'pre_filter': 'reversed',
                                'post_filter': 'reversed',
                                'min_word_length': 3,
                                'prefix_length': 3
                            }
                        ],
                        'collate': {
                            'query': {
                                'source': {
                                    'match': {
                                        'topics_and_skills': '{{suggestion}}'
                                    }
                                }
                            },
                            'prune': False
                        },
                        'smoothing': {
                            "stupid_backoff": {
                                "discount": 0.4
                            }
                        }
                    }
                }
            }
        }
        return query

    def _make_aggregation_query(self):
        """Make a query for count aggregations of the search results.

        Counts are generated for these fields: format, platform and content length.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        query = {
            'aggs': {
                'content_format': {
                    'terms': {
                        'field': 'content_format'
                    }
                },
                'platform': {
                    'terms': {
                        'field': 'platform'
                    }
                },
                'content_length_range': {
                    'range': {
                        'field': 'content_length',
                        'ranges': [
                            {'to': 1.0},
                            {'from': 1.0, 'to': 5.0},
                            {'from': 5.0}
                        ]
                    }
                }
            }
        }
        return query

    def _make_post_filter_query(self, post_filters):
        """Make a query for the post filter feature.

        Post filter is used to narrow down results after a search.

        Parameters
        ----------
        post_filters : dict
            The acceptable keys are `content_format`, `platform`, `content_length_range`.
            Value of a key is a `list`.

        Returns
        -------
        query : dict
           Request body that can be sent to Elasticsearch.
        """
        if not post_filters:
            return {}

        for filter_ in post_filters:
            if filter_ not in {'content_format', 'platform', 'content_length_range'}:
                raise KeyError(f'{filter} is an unexpected key')

        post_filter_bool = []
        for filter_, values in post_filters.items():
            if filter_ == 'content_length_range':
                for value in values:
                    if value == (None, 1):
                        post_filter_bool.append({'range': {'content_length': {'lt': 1.0}}})
                    elif value == (1, 5):
                        post_filter_bool.append({'range': {'content_length': {'gte': 1.0, 'lt': 1.0}}})
                    else:
                        post_filter_bool.append({'range': {'content_length': {'gte': 5.0}}})
            else:
                post_filter_bool.append({'terms': {filter_: values}})

        query = {
            'post_filter': {
                'bool': {
                    'should': post_filter_bool
                }
            }
        }
        return query

    def make_query(self, user_query, is_auto_complete, must_not=None, filters=None, post_filters=None, size=10):
        """Make query for course searching.

        Parameters
        ----------
        user_query : str
            Input from the user.
        is_auto_complete : bool
            Whether or not the query is for the auto-completion feature.
        must_not : dict or None
            Properties of courses that should not appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        filters : dict or None
            Properties of courses that should appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        post_filters : dict or None
            The acceptable keys are `content_format`, `platform`, `content_length_range`.
            Value of a key is a `list`.
        size : int
            The maximum number of items to return from the search engine.

        Returns
        -------
        query : dict
           Request body that can be sent to Elasticsearch.
        """
        if is_auto_complete:
            return self._make_auto_com_query(user_query)

        search_query = self._make_search_query(user_query)
        did_you_mean_query = self._make_did_you_mean_query(user_query)
        aggregation_query = self._make_aggregation_query()
        query = {**search_query, **did_you_mean_query, **aggregation_query}
        query['size'] = size

        if filters is not None:
            if 'filter' not in query['query']['bool']:
                query['query']['bool']['filter'] = []
            for field, values in filters.items():
                query['query']['bool']['filter'].append({'terms': {field + '.key': values}})
        if must_not is not None:
            if 'must_not' not in query['query']['bool']:
                query['query']['bool']['must_not'] = []
            for field, values in must_not.items():
                query['query']['bool']['must_not'].append({'terms': {field + '.key': values}})
        if post_filters is not None:
            post_filter_query = self._make_post_filter_query(post_filters)
            query.update(post_filter_query)
        return query

